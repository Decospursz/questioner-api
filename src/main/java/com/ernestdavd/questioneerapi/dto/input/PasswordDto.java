package com.ernestdavd.questioneerapi.dto.input;

import com.ernestdavd.questioneerapi.validators.interfaces.EqualsTo;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@EqualsTo(field1 = "newPassword", field2 = "confirmPassword", message = "{user.newPassword.matchConfirm}", groups = PasswordDto.Third.class)
@GroupSequence({
        PasswordDto.First.class,
        PasswordDto.Second.class,
        PasswordDto.Third.class,
        PasswordDto.class
})
public class PasswordDto {
    @NotNull(message = "{user.oldPassword.notEmpty}", groups = First.class)
    @Size(min = 5, message = "{user.oldPassword.sizeError}", groups = Second.class)
    private String oldPassword;
    @NotNull(message = "{user.newPassword.notEmpty}", groups = First.class)
    @Size(min = 5, message = "{user.newPassword.sizeError}", groups = Second.class)
    private String newPassword;
    @NotNull(message = "{user.confirmPassword.notEmpty}", groups = First.class)
    @Size(min = 5, message = "{user.confirmPassword.sizeError}", groups = Second.class)
    private String confirmPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }
}
