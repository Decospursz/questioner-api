package com.ernestdavd.questioneerapi.dto.output;

import com.ernestdavd.questioneerapi.dto.enums.Status;
import com.ernestdavd.questioneerapi.model.User;

public class UserResponseDTO extends StandardResponseDTO {

    private User user;

    public UserResponseDTO(Status status, User user) {
        super(status);
        this.user = user;
    }

    public UserResponseDTO(Status status) {
        this(status, null);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
